## Yakforms

A Docker image for [Yakforms](https://yakforms.org/), a « free-as-in-free-speech software that allows you to easily create forms for your community ».

## Content

The Docker image is really PHP-FPM with additional modules enabled and Drupal distribution ready to be set up.

## Usage

To run Yakforms, you will need :
- A PHP-FPM container (provided by this image)
- A database container (we chose PostgreSQL)
- A web-server (we chose nginx)

A minimal Docker Compose file would look like this :

```yaml
version: '3.7'

volumes:
  db:
  data:

services:
  front:
    image: 'nginx:1.21'
    container_name: 'yakforms_nginx'
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf:ro
      - data:/var/www/yakforms
    ports:
      - 80:80

  app:
    image: 'yakforms:1.1'
    build: .
    container_name: 'yakforms_app'
    volumes:
      - data:/var/www/yakforms

  db:
    image: 'postgres:14'
    container_name: 'yakforms_db'
    volumes:
      - db:/var/lib/postgresql
    environment:
      POSTGRES_PASSWORD:
      POSTGRES_USER:
      POSTGRES_DB:
```

The `nginx.conf` depends on your setup. If you aren't behind a reverse-proxy, you'll want to add SSL capabilities to nginx.
See the [Yakforms installation documentation](https://yakforms.org/pages/install.html) for a sample `nginx.conf` and [the nginx configuration](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/) to understand how to make Yakforms accessible through HTTPS.

Fill database environment variables, then run :

```bash
docker-compose build
docker-compose up -d
docker-compose logs -f
```

## First start

Navigate to the home page of your website and follow the [installation instructions](https://yakforms.org/pages/install.html) from point 4.

Yakforms specific configuration is done from `<your_website_url>/admin/config/system/yakforms`.

## Upgrade

Simply use a newer version of the image and the upgrade will take place when the container is recreated, *e.g.* :

```yaml
image: 'yakforms:1.2'
```

If you are building from source, check that `YAKFORMS_VERSION` is the version you want in the [Dockerfile](./Dockerfile).
