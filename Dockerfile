FROM php:7.4-fpm-alpine3.14
ENV YAKFORMS_VERSION=1.1

# Install dependencies
RUN apk add oniguruma-dev postgresql-dev libxml2-dev libpng-dev libzip-dev wget unzip

# Install PHP modules needed for Yakforms
RUN docker-php-ext-install mbstring pdo_pgsql gd simplexml xml zip

# We will install Yakforms here
WORKDIR /var/www/yakforms

# Get release and unzip in temporary location.
# The content of release will be copied onto /var/www/yakforms
# at runtime, because an existing version could be mounted on it.
# This way, we only update need files and keep old ones.
RUN wget -O /tmp/yakforms.zip https://packages.yakforms.org/yakforms_distribution_${YAKFORMS_VERSION}.zip && \
    unzip -d / /tmp/yakforms.zip

# With the above, all files under /var/www/yakforms should be persistent
VOLUME ["/var/www/yakforms"]

# The entrypoint takes care of copying sample configuration
# for the first time, and copying the release.
COPY start.sh /start.sh
RUN chmod +x /start.sh
ENTRYPOINT ["/start.sh"]

# Finally, run PHP. php-fpm will run with www-data user.
CMD ["php-fpm"]
