#!/bin/sh

# Copy new release onto old files
echo "Copy release ${YAKFORMS_VERSION}..."
cp -r /yakforms_distribution/* /var/www/yakforms/

# Create configuration file if it does not exist
if [ -e /var/www/yakforms/sites/default/default.settings.php ]
then
  echo "Configuration file not found, copy thee default settings..."
  cp /var/www/yakforms/sites/default/default.settings.php /var/www/yakforms/sites/default/settings.php
fi

# php-fpm runs as www-data
echo "Ensure /var/www/yakforms belongs to www-data..."
chown -R www-data:www-data /var/www/yakforms

# Execute php-fpm
echo "Run $@..."
exec $@
